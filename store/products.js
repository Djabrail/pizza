export const state = () => ({
  list: [
    {
      id: 1,
      title: 'Пепперони фреш',
      img: 'https://dodopizza-a.akamaihd.net/static/Img/Products/e9624d2bf1ae41598cd6635c0d3ed0f6_292x292.jpeg',
      price: 245,
      category: 'Мясные',
    },
    {
      id: 2,
      title: 'Ветчина и сыр',
      img: 'https://dodopizza-a.akamaihd.net/static/Img/Products/Pizza/ru-RU/679924dc-e4fd-45fd-aceb-be139f265425.jpg',
      price: 295,
      category: 'Вегетарианская',
    },
    {
      id: 3,
      title: 'Двойной цыпленок',
      img: 'https://dodopizza-a.akamaihd.net/static/Img/Products/dd4b719911d048e0b05c3e4219880e64_292x292.jpeg',
      price: 295,
      category: 'Вегетарианская',
    },
    {
      id: 4,
      title: 'Гавайская',
      img: 'https://dodopizza-a.akamaihd.net/static/Img/Products/4749ca7de7e247daa69a685714f06aa3_292x292.jpeg',
      price: 375,
      category: 'Гриль',
    },
    {
      id: 5,
      title: 'Сырный цыпленок',
      img: 'https://dodopizza-a.akamaihd.net/static/Img/Products/36785e0eea834174bf358ac2682aea4d_292x292.jpeg',
      price: 425,
      category: 'Гриль',
    },
    {
      id: 6,
      title: 'Цыпленок ранч',
      img: 'https://dodopizza-a.akamaihd.net/static/Img/Products/0665a05313b1431588acec0ecfac0888_292x292.jpeg',
      price: 425,
      category: 'Острые',
    },
    {
      id: 7,
      title: 'Мясная',
      img: 'https://dodopizza-a.akamaihd.net/static/Img/Products/b10dd95184eb49d39a739d75b8cbfeaf_292x292.jpeg',
      price: 425,
      category: 'Острые',
    },
    {
      id: 8,
      title: 'Аррива!',
      img: 'https://dodopizza-a.akamaihd.net/static/Img/Products/a2407468a4094e498d258f65bffa4c46_292x292.jpeg',
      price: 375,
      category: 'Закрытые',
    },
  ],
})
